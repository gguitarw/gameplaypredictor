import csv
import random
import numpy as np
from tqdm import tqdm

def load_crit_points(file_path):
    print('Loading crit points from \'{}\'... '.format(file_path), end='', flush=True)
    crit_points = []

    # add a random tick at the beginning so we always have a previous crit point to reference
    crit_points.append({'time':int(-1001),
                        'coord_x': float(random.randint(0, 512)),
                        'coord_y': float(random.randint(0, 384)),
                        'type': int(1)})

    with open(file_path) as f:
        reader = csv.reader(f)
        for row in reader:
            crit_point = {'time': int(row[0]),
                          'coord_x': float(row[1]),
                          'coord_y': float(row[2]),
                          'type': int(row[3])}
            crit_points.append(crit_point)
    
    # add random ticks at the end so as not to run out of crit points
    crit_points.append({'time': crit_points[len(crit_points) - 1]['time'] + 1001,
                        'coord_x': float(random.randint(0, 512)),
                        'coord_y': float(random.randint(0, 384)),
                        'type': int(1)})
    crit_points.append({'time': crit_points[len(crit_points) - 1]['time'] + 1201,
                        'coord_x': float(random.randint(0, 512)),
                        'coord_y': float(random.randint(0, 384)),
                        'type': int(1)})

    print('DONE')
    return crit_points

def keep_within_range(coord_x, coord_y, x_low=0, x_high=512, y_low=0, y_high=384):
    # TODO: could pass tuples
    if coord_x < x_low:
        coord_x = x_low
    if coord_x > x_high:
        coord_x = x_high
    if coord_y < y_low:
        coord_y = y_low
    if coord_y > y_high:
        coord_y = y_high
    return coord_x, coord_y

# returns gameplay_ticks list
def generate_gameplay(model, crit_points):
    #print('Generating gameplay... ', end='', flush=True)
    gameplay_ticks = []

    coord_x = float(random.randint(0, 512))
    coord_y = float(random.randint(0, 384))
    init_time = int(crit_points[1]['time']) - 1000  # start one second before first crit 

    next_crit_index = 1

    for time in tqdm(range(init_time, crit_points[len(crit_points) - 2]['time']), desc='Generating gameplay'):
        if time >= crit_points[next_crit_index]['time']:
            next_crit_index += 1
            
        previous = crit_points[next_crit_index - 1]
        next = crit_points[next_crit_index]
        nextnext = crit_points[next_crit_index + 1]
        
        visible = 0;
        # TODO: somehow get map's AR to this point so we can use it to process object visibility
        if (time >= next['time'] - 425):  # 425 is time before click that it's visible
            visible = 1

        feature = [coord_x,
                   coord_y,
                   previous['coord_x'] - coord_x,
                   previous['coord_y'] - coord_y,
                   next['coord_x'] - coord_x,
                   next['coord_y'] - coord_y,
                   nextnext['coord_x'] - coord_x,
                   nextnext['coord_y'] - coord_y,
                   next['time'] - time,
                   nextnext['time'] - time,
                   (time - previous['time']) / (next['time'] - time),
                   visible]

        change_x, change_y = model.predict(np.array([np.asarray(feature)]))[0]

        coord_x += change_x
        coord_y += change_y

        # limit cursor to the playfield (not actually what happens in gameplay)
        coord_x, coord_y = keep_within_range(coord_x, coord_y)

        gameplay_ticks.append({'time': int(time),
                               'coord_x': float(coord_x),
                               'coord_y': float(coord_y)})
    #print('DONE')
    return gameplay_ticks

# TODO: make functions consistent with others, as in appending a file type and what not
def write_gameplay_ticks_to_file(gameplay_ticks, output_path):
    with open(output_path, 'w') as f:
        for tick in gameplay_ticks:
            line = str(tick['time']) + ',' + str(tick['coord_x']) + ',' + str(tick['coord_y']) + '\n'
            f.write(line)

def load_gameplay_ticks_from_file(input_path):
    gameplay_ticks = []
    with open(input_path) as f:
        reader = csv.reader(f)
        for row in reader:
            gameplay_tick = {'time': int(row[0]),
                             'coord_x': float(row[1]),
                             'coord_y': float(row[2])}
            gameplay_ticks.append(gameplay_tick)
    return gameplay_ticks
