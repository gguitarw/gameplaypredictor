import cv2
import numpy as np
import os
from tqdm import tqdm

# TODO: enum for this or something
__CRIT_CIRCLE = 1
__CRIT_SLIDER = 2
__CRIT_SLIDER_END = 4
__CRIT_SLIDER_TICK = 8
__CRIT_SLIDER_END = 16
__CRIT_SPINNER = 32
__CRIT_SPINNER_START = 64
__CRIT_SPINNER_END = 128


# returns a tuple of the coordinates tuple (top-left and bottom-right of rectangle)
# TODO: should be get_playfield_data
def get_playfield_coords(window_size):
    playfield_scale = (window_size[1] - (window_size[1] / 5)) / 384
    playfield_w = int(512 * playfield_scale)
    playfield_h = int(384 * playfield_scale)

    playfield_x = int((window_size[0] - playfield_w) / 2)
    playfield_y = int((window_size[1] - playfield_h) / 2)

    return ((playfield_x, playfield_y), (playfield_x + playfield_w, playfield_y + playfield_h), playfield_scale)

def render(gameplay_ticks,
           output_name='video_output\output.avi',
           img_size=(640, 480),
           fps=60.0,
           draw_playfield_border=True,
           use_skin_cursor=False,
           use_cursor_trail=True, cursor_trail_length=3,
           critical_points=None):
    #print('Rendering video... ', end='', flush=True)
    fourcc = cv2.VideoWriter_fourcc('h', '2', '6', '4')
    out = cv2.VideoWriter(output_name, fourcc, fps, img_size)

    cursor_img = None
    if use_skin_cursor:
        #cursor_img_path = 'resources\\cursors\\angelsim_black-bg.png'
        cursor_img_path = 'resources\\cursors\\seouless_momoko_black-bg.png'
        cursor_img = cv2.imread(cursor_img_path)

    q = []
    crits = []  # TODO: refactor these names for critical points
    crit_index = 0
    crit_num_to_display = 2

    # TODO: ensure that we maintain 1000 ticks per 60 frames (per second)
    tick_index = 0
    length_in_seconds = int(gameplay_ticks[-1]['time'] / 1000 - gameplay_ticks[0]['time'] / 1000)
    for second in tqdm(range(length_in_seconds)):
        ticks_per_second = 1000
        frames_per_second = int(fps)  # usually 60
        ticks_this_second = 0
        frames_this_second = 0
        for frame_num in range(frames_per_second):
            frames_to_do = float(frames_per_second - frames_this_second)
            ticks_to_do = float(ticks_per_second - ticks_this_second)
            milliseconds_to_process = int(round(ticks_to_do / frames_to_do))
            # really just skip ahead every milliseconds_to_process ticks

            tick = gameplay_ticks[tick_index]

            if critical_points is not None:
                # update crit_index
                while tick['time'] > critical_points[crit_index]['time']:
                    crit_index += 1
                # TODO: optimize this so as not to clear and re-add
                crits.clear()
                for crit_point in critical_points[crit_index:crit_index + crit_num_to_display]:
                    crits.append(crit_point)

            img = get_image([tick], img_size, cursor_img=cursor_img, cursor_trail=q, critical_points=crits)
        
            if use_cursor_trail:
                # update tail points (push out last point, add new point)
                while len(q) >= cursor_trail_length:
                    q.pop()
                q.insert(0, tick)

            out.write(img)

            tick_index += milliseconds_to_process
            ticks_this_second += milliseconds_to_process
            frames_this_second += 1

    #for tick in tqdm(gameplay_ticks[::16],
    #                 desc='Rendering video at {}x{}'.format(img_size[0], img_size[1]),
    #                 unit='frames',
    #                 ascii=True):

    #    if critical_points is not None:
    #        # update crit_index
    #        while tick['time'] > critical_points[crit_index]['time']:
    #            crit_index += 1
    #        # TODO: optimize this so as not to clear and re-add
    #        crits.clear()
    #        for crit_point in critical_points[crit_index:crit_index + crit_num_to_display]:
    #            crits.append(crit_point)

    #    img = get_image([tick], img_size, cursor_img=None, cursor_trail=q, critical_points=crits)
        
    #    if use_cursor_trail:
    #        # update tail points (push out last point, add new point)
    #        while len(q) >= cursor_trail_length:
    #            q.pop()
    #        q.insert(0, tick)

    #    out.write(img)

    out.release()
    print('Finished writing video to \'{}\''.format(output_name))
    #print('DONE')

def render_2(gameplays,
             output_name='video_output\output.avi',
             img_size=(640, 480),
             fps=60.0,
             draw_playfield_border=True,
             use_skin_cursor=False,
             use_cursor_trail=True, cursor_trail_length=3,
             critical_points=None):
    #print('Rendering video... ', end='', flush=True)
    fourcc = cv2.VideoWriter_fourcc('h', '2', '6', '4')
    out = cv2.VideoWriter(output_name, fourcc, fps, img_size)

    cursor_img = None
    if use_skin_cursor:
        #cursor_img_path = 'resources\\cursors\\angelsim_black-bg.png'
        cursor_img_path = 'resources\\cursors\\seouless_momoko_black-bg.png'
        cursor_img = cv2.imread(cursor_img_path)

    q = []
    crits = []  # TODO: refactor these names for critical points
    crit_index = 0
    crit_num_to_display = 2

    # TODO: ensure that we maintain 1000 ticks per 60 frames (per second)
    for gameplay in gameplays:
        gameplay_ticks = gameplay[0]
        cursor_trail = gameplay[1]
        for tick in tqdm(gameplay_ticks[::16],
                         desc='Rendering video at {}x{}'.format(img_size[0], img_size[1]),
                         unit='frames',
                         ascii=True):

            if critical_points is not None:
                # update crit_index
                while tick['time'] > critical_points[crit_index]['time']:
                    crit_index += 1
                # TODO: optimize this so as not to clear and re-add
                crits.clear()
                for crit_point in critical_points[crit_index:crit_index + crit_num_to_display]:
                    crits.append(crit_point)

            img = get_image([tick], img_size, cursor_img=None, cursor_trail=q, critical_points=crits)
            
            if use_cursor_trail:
                # update tail points (push out last point, add new point)
                while len(q) >= cursor_trail_length:
                    q.pop()
                q.insert(0, tick)

        out.write(img)

    out.release()
    print('Finished writing video to \'{}\''.format(output_name))
    #print('DONE')


def render_multiple(gameplays,  # each gameplay contains a list of ticks with information on time and coords
                    img_size,
                    fps,
                    draw_playfield_border=True,
                    color_each_play=True):
    output_name = 'output'.avi  # TODO: parameter for this
    fourcc = cv2.VideoWriter_fourcc('h', '2', '6', '4')
    out = cv2.VideoWriter(output_name, fourcc, fps, img_size)

    num_milliseconds_in_gameplay = gameplays[0][len(gameplays[0]) - 1]['time']
    #for ...


def overlay_image(src, overlay, xpos, ypos):
    # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_core/py_image_arithmetics/py_image_arithmetics.html
    img1 = src.copy()
    img2 = overlay.copy()
    #cv2.imshow('img1', img1)
    #cv2.imshow('img2', img2)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()

    rows, cols, channels = img2.shape
    roi = img1[ypos:ypos+rows, xpos:xpos+cols]
    img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)
    img1_bg = cv2.bitwise_and(roi, roi, mask = mask_inv)
    img2_fg = cv2.bitwise_and(img2, img2, mask = mask)
    dst = cv2.add(img1_bg, img2_fg)
    img1[ypos:ypos+rows, xpos:xpos+cols] = dst
    return img1
    #cv2.imshow('res', img1)
    #cv2.imwrite('result.png', img1)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()

# returns an opencv image
def get_image(gameplay_ticks, img_size, draw_playfield_border=True,
              cursor_color=(255, 255, 255), cursor_img=None, cursor_trail=None,
              critical_points=None):
    # TODO: if only one gameplay tick was input, change the shape so it's in a list and can be used later
    #       ^^^ same with cursor trail?

    # TODO: should pass in `gameplays` which is a list of tuples: (gameplay ticks, cursor trail)
            # put cursor trail code inside `for cursor in gameplays`

    img = np.zeros((img_size[1], img_size[0], 3), np.uint8)
    pf_topleft, pf_bottomright, playfield_scale = get_playfield_coords(img_size)
    if draw_playfield_border:
        # fill with gray, then fill playfield with black again
        img[:] = (16, 16, 16)
        cv2.rectangle(img, pf_topleft, pf_bottomright, (0, 0, 0), -1)
        
    # draw next critical point
    if critical_points is not None and len(critical_points) > 0:
        crit_count = len(critical_points)
        for i, critical_point in enumerate(critical_points):
            crit_coord = (int(round(critical_point['coord_x'] * playfield_scale)) + int(pf_topleft[0]),
                          int(round(critical_point['coord_y'] * playfield_scale)) + int(pf_topleft[1]))
            crit_shade = int((255 / (crit_count)) * (crit_count - i))
            type = critical_point['type']
            circle_thickness = int(1 * playfield_scale)
            if type & __CRIT_CIRCLE or type & __CRIT_SLIDER:
                circle_thickness = int(3 * playfield_scale)
                crit_color = (0, 0, crit_shade)  # red
            if type & __CRIT_SLIDER_TICK or type & __CRIT_SLIDER_END:
                circle_thickness = int(1 * playfield_scale)
                crit_color = (crit_shade, 0, 0)  # blue
            cv2.circle(img, crit_coord, int(20 * playfield_scale), crit_color, circle_thickness)
            # TODO: different shapes for different types of crit point (circle, slider, tick, end, etc...)
            # TODO: allow passing multiple ticks and it will process them similar to cursor trail, fade out if its further in future

    cursor_radius = int(2 * playfield_scale)
    use_line_trail = True
    # draw cursor trail before cursor, from last trail tick to first to keep layering correct
    if cursor_trail is not None and len(cursor_trail) > 0:
        last_tick = gameplay_ticks[0]
        trail_count = len(cursor_trail)
        for i, trail_tick in enumerate(reversed(cursor_trail)):  # reverse order, draw darkest color first
            last_tick_coord = (int(round(last_tick['coord_x'] * playfield_scale)) + int(pf_topleft[0]),
                               int(round(last_tick['coord_y'] * playfield_scale)) + int(pf_topleft[1]))
            trail_tick_coord = (int(round(trail_tick['coord_x'] * playfield_scale)) + int(pf_topleft[0]),
                                int(round(trail_tick['coord_y'] * playfield_scale)) + int(pf_topleft[1]))
            # TODO: implement using a cursor_trail.png with level adjustments to make it darker the older it is
            #       also instead of using grayscale be able to adjust different colors, so scale cursor_color by color_shade_scale (doesn't exist yet)
            trail_shade = int((255 / (trail_count + 1)) * (i + 1))
            tick_color = (trail_shade, trail_shade, trail_shade)
            if use_line_trail:
                cv2.line(img, last_tick_coord, trail_tick_coord, tick_color, cursor_radius)
            else:
                cv2.circle(img, trail_tick_coord, cursor_radius, tick_color, -1)
            last_tick = trail_tick

    # draw current cursor position
    for cursor in gameplay_ticks:
        # TODO: implement using a different color/skin for each tick
        circle_center_coord = (int(round(cursor['coord_x'] * playfield_scale)) + int(pf_topleft[0]),
                               int(round(cursor['coord_y'] * playfield_scale)) + int(pf_topleft[1]))
        if cursor_img is not None:
            cursor_rows, cursor_cols, cursor_channels = cursor_img.shape
            circle_topleft_coord = (int(circle_center_coord[0] - cursor_cols / 2),
                                    int(circle_center_coord[1] - cursor_rows / 2))
            img = overlay_image(img, cursor_img, circle_topleft_coord[0], circle_topleft_coord[1])
        else:
            cv2.circle(img, circle_center_coord, cursor_radius, cursor_color, -1)

    return img

# display the "OpenH264 Video Codec provided by Cisco Systems, Inc." message upon importing render.py
__cv2_h264 = cv2.VideoWriter_fourcc('h', '2', '6', '4')
__cv2_t = cv2.VideoWriter('__test_.avi', __cv2_h264, 60.0, (640, 480))
__cv2_t.release()
os.remove('__test_.avi')