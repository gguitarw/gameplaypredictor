import tensorflow as tf
import numpy as np
#import tensorflow.keras as keras
import keras
import os
import csv
from tqdm import tqdm

# returns (feature_data, label_data) arrays
def load_dataset(file_path):
    print('Loading dataset... ', end='')
    feature_data = []
    label_data = []

    FEATURE_COLUMNS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    LABEL_COLUMNS = [15, 16]

    with open(file_path) as f:
        reader = csv.reader(f)
        next(reader)  # skip header

        # TODO optimize this to avoid extra copying
        data = []
        for row in reader:
            data.append(row)
        np.random.shuffle(data)

        for row in tqdm(data, desc='Loading dataset'):
            features = [row[i] for i in FEATURE_COLUMNS]
            labels = [row[j] for j in LABEL_COLUMNS]
            feature_data.append(features)
            label_data.append(labels)

        # shuffle should be done here, meaning we need to keep features and labels together until returning

    print('DONE')
    return (np.asarray(feature_data), np.asarray(label_data))


from keras.layers import Dense
from keras.layers import Dropout
def define_model(num_features, num_labels, layers=[(None, 'relu'), (100, 'relu')], 
                 optimizer='adam', loss='mean_squared_error',
                 use_dropout=True, dropout_rate=0.2):
    model = keras.models.Sequential()

    # TODO: could specify dropout layer specifically like 'dropout', similar to 'relu', and the first part would be the rate

    # input layer
    model.add(Dense(num_features, activation=layers[0][1]))
    
    # hidden layers
    for nodes, activation in layers[1:]:
        model.add(Dense(nodes, activation=activation))
        if use_dropout:
            model.add(Dropout(dropout_rate))

    # remove the last dropout layer
    if use_dropout:
        model.layers.pop()
    # add output layer
    model.add(Dense(num_labels))
    model.compile(optimizer=optimizer, loss=loss)

    # TODO: output the summary to a unique file name to get information later
    return model


def train_model(model, features, labels, epochs,
                use_validation=True, validation_split=0.2):
    if use_validation:
        model.fit(features, labels, epochs=epochs, validation_split=validation_split)
    else:
        model.fit(features, labels, epochs=epochs)

    # TODO: look into saving the history from history=model.fit() to a log for training time
    
    model.summary()
    return model


def save_model(model, file_name, output_directory=None):
    if output_directory:
        model.save(os.path.join(output_directory, file_name + '.h5'))
    else:
        model.save(file_name + '.h5')

def load_model(file_path):
    if '.' not in file_path:
        file_path += '.h5'
    return keras.models.load_model(file_path)
