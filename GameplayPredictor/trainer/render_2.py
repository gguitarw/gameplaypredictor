import cv2
import numpy as np
import os
from tqdm import tqdm

# TODO: enum for this or something
__CRIT_CIRCLE = 1
__CRIT_SLIDER = 2
__CRIT_SLIDER_END = 4
__CRIT_SLIDER_TICK = 8
__CRIT_SLIDER_END = 16
__CRIT_SPINNER = 32
__CRIT_SPINNER_START = 64
__CRIT_SPINNER_END = 128


# returns a tuple of the coordinates tuple (top-left and bottom-right of rectangle)
# TODO: should be get_playfield_data
def get_playfield_coords(window_size):
    playfield_scale = (window_size[1] - (window_size[1] / 5)) / 384
    playfield_w = int(512 * playfield_scale)
    playfield_h = int(384 * playfield_scale)

    playfield_x = int((window_size[0] - playfield_w) / 2)
    playfield_y = int((window_size[1] - playfield_h) / 2)

    return ((playfield_x, playfield_y), (playfield_x + playfield_w, playfield_y + playfield_h), playfield_scale)

def render(gameplay_ticks,
           output_name='video_output\output.avi',
           img_size=(640, 480),
           fps=60.0,
           draw_playfield_border=True,
           use_skin_cursor=False,
           use_cursor_trail=True, cursor_trail_length=3,
           critical_points=None):
    #print('Rendering video... ', end='', flush=True)
    fourcc = cv2.VideoWriter_fourcc('h', '2', '6', '4')
    out = cv2.VideoWriter(output_name, fourcc, fps, img_size)

    cursor_img = None
    if use_skin_cursor:
        #cursor_img_path = 'resources\\cursors\\angelsim_black-bg.png'
        cursor_img_path = 'resources\\cursors\\seouless_momoko_black-bg.png'
        cursor_img = cv2.imread(cursor_img_path)

    q = []
    crits = []  # TODO: refactor these names for critical points
    crit_index = 0
    crit_num_to_display = 2

    # TODO: ensure that we maintain 1000 ticks per 60 frames (per second)
    for tick in tqdm(gameplay_ticks[::16],
                     desc='Rendering video at {}x{}'.format(img_size[0], img_size[1]),
                     unit='frames',
                     ascii=True):

        if critical_points is not None:
            # update crit_index
            while tick['time'] > critical_points[crit_index]['time']:
                crit_index += 1
            # TODO: optimize this so as not to clear and re-add
            crits.clear()
            for crit_point in critical_points[crit_index:crit_index + crit_num_to_display]:
                crits.append(crit_point)

        img = get_image([tick], img_size, cursor_img=None, cursor_trail=q, critical_points=crits)
            
        if use_cursor_trail:
            # update tail points (push out last point, add new point)
            while len(q) >= cursor_trail_length:
                q.pop()
            q.insert(0, tick)

        out.write(img)

    out.release()
    print('Finished writing video to \'{}\''.format(output_name))
    #print('DONE')

def render_2(gameplays,
             output_name='video_output\output.avi',
             img_size=(640, 480),
             fps=60.0,
             draw_playfield_border=True,
             use_skin_cursor=False,
             use_cursor_trail=True, cursor_trail_length=3,
             critical_points=None):

    #print('Rendering video... ', end='', flush=True)
    fourcc = cv2.VideoWriter_fourcc('h', '2', '6', '4')
    out = cv2.VideoWriter(output_name, fourcc, fps, img_size)

    cursor_img = None
    if use_skin_cursor:
        #cursor_img_path = 'resources\\cursors\\angelsim_black-bg.png'
        cursor_img_path = 'resources\\cursors\\seouless_momoko_black-bg.png'
        cursor_img = cv2.imread(cursor_img_path)

    q = []
    crits = []  # TODO: refactor these names for critical points
    crit_index = 0
    crit_num_to_display = 2

    # this is all assuming the gameplays' ticks have the same 'time' values
    # TODO: eventually, this should keep track by gametime, not index, because gameplays could start/end at different times (this would be much slower probably)
    total_frames = int((gameplays[0][-1]['time'] - gameplays[0][0]['time']) * fps / 1000)
    frames_per_second = int(fps)
    ticks_per_second = 1000  # 1 tick every millisecond
    tick_index = 0
    ticks_this_second = 0
    frames_this_second = 0

    frame_count = 0
    ms_per_frame_history = []

    for frame in tqdm(range(total_frames), desc='Rendering video at {}x{}'.format(img_size[0], img_size[1]), ascii=True, unit='frames'):
        if frames_this_second >= 60:
            frames_this_second = 0
            ticks_this_second = 0

        milliseconds_per_frame = int(round(float(ticks_per_second - ticks_this_second) / float(frames_per_second - frames_this_second)))
        ms_per_frame_history.append(milliseconds_per_frame)

        # TODO: get_image stuff here
        if tick_index >= len(gameplays[0]):
            continue
        tick_time = gameplays[0][tick_index]['time']

        if critical_points is not None:
            while tick_time > critical_points[crit_index]['time']:
                crit_index += 1
            # TODO: optimize so as not to clear and re-add every time?
            crits = [crit_point for crit_point in critical_points[crit_index:crit_index + crit_num_to_display]]

        tick_gameplays = []
        for gameplay in gameplays:
            tick_gameplay = [gameplay[tick_index]]  # the cursor itself
            if use_cursor_trail:
                trail_ms_history = [ms for ms in ms_per_frame_history[:-cursor_trail_length:-1]]
                # TODO: this doesn't quite follow the path since it will be off by one if ms/tick is 16 and previous was 17
                trail_index = tick_index
                for skip_distance in trail_ms_history:
                    trail_index -= skip_distance
                    tick_gameplay.append(gameplay[trail_index])
                #tick_gameplay.extend([trail_tick for trail_tick in gameplay[tick_index:
                #                                                            tick_index - (cursor_trail_length * milliseconds_per_frame):
                #                                                            -milliseconds_per_frame]])
            tick_gameplays.append(tick_gameplay)

        img = get_image(tick_gameplays, img_size, cursor_img=cursor_img, critical_points=crits)

        out.write(img)

        tick_index += milliseconds_per_frame
        ticks_this_second += milliseconds_per_frame
        frames_this_second += 1
        frame_count += 1


    # TODO: ensure that we maintain 1000 ticks per 60 frames (per second)
    #for tick in tqdm(gameplay_ticks[::16],
    #                 desc='Rendering video at {}x{}'.format(img_size[0], img_size[1]),
    #                 unit='frames',
    #                 ascii=True):

    #    if critical_points is not None:
    #        # update crit_index
    #        while tick['time'] > critical_points[crit_index]['time']:
    #            crit_index += 1
    #        # TODO: optimize this so as not to clear and re-add
    #        crits.clear()
    #        for crit_point in critical_points[crit_index:crit_index + crit_num_to_display]:
    #            crits.append(crit_point)

    #    img = get_image([tick], img_size, cursor_img=None, cursor_trail=q, critical_points=crits)
            
    #    if use_cursor_trail:
    #        # update tail points (push out last point, add new point)
    #        while len(q) >= cursor_trail_length:
    #            q.pop()
    #        q.insert(0, tick)

    #    out.write(img)

    out.release()
    print('Finished writing video to \'{}\''.format(output_name))
    #print('DONE')


def render_multiple(gameplays,  # each gameplay contains a list of ticks with information on time and coords
                    img_size,
                    fps,
                    draw_playfield_border=True,
                    color_each_play=True):
    output_name = 'output'.avi  # TODO: parameter for this
    fourcc = cv2.VideoWriter_fourcc('h', '2', '6', '4')
    out = cv2.VideoWriter(output_name, fourcc, fps, img_size)

    num_milliseconds_in_gameplay = gameplays[0][len(gameplays[0]) - 1]['time']
    #for ...


def overlay_image(src, overlay, xpos, ypos):
    # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_core/py_image_arithmetics/py_image_arithmetics.html
    img1 = src.copy()
    img2 = overlay.copy()
    #cv2.imshow('img1', img1)
    #cv2.imshow('img2', img2)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()

    rows, cols, channels = img2.shape
    roi = img1[ypos:ypos+rows, xpos:xpos+cols]
    img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)
    img1_bg = cv2.bitwise_and(roi, roi, mask = mask_inv)
    img2_fg = cv2.bitwise_and(img2, img2, mask = mask)
    dst = cv2.add(img1_bg, img2_fg)
    img1[ypos:ypos+rows, xpos:xpos+cols] = dst
    return img1
    #cv2.imshow('res', img1)
    #cv2.imwrite('result.png', img1)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()

# returns an opencv image
def get_image(gameplays, img_size, draw_playfield_border=True,
              cursor_color=(255, 255, 255), cursor_img=None,
              critical_points=None):
    # TODO: if only one gameplay tick was input, change the shape so it's in a list and can be used later
    #       ^^^ same with cursor trail?

    # TODO: should pass in `gameplays` which is a list of tuples: (gameplay ticks, cursor trail)
            # put cursor trail code inside `for cursor in gameplays`

    img = np.zeros((img_size[1], img_size[0], 3), np.uint8)
    pf_topleft, pf_bottomright, playfield_scale = get_playfield_coords(img_size)
    if draw_playfield_border:
        # fill with gray, then fill playfield with black again
        img[:] = (16, 16, 16)
        cv2.rectangle(img, pf_topleft, pf_bottomright, (0, 0, 0), -1)
        
    # draw next critical point
    if critical_points is not None and len(critical_points) > 0:
        crit_count = len(critical_points)
        for i, critical_point in enumerate(critical_points):
            crit_coord = (int(round(critical_point['coord_x'] * playfield_scale)) + int(pf_topleft[0]),
                          int(round(critical_point['coord_y'] * playfield_scale)) + int(pf_topleft[1]))
            crit_shade = int((255 / (crit_count)) * (crit_count - i))
            type = critical_point['type']
            circle_thickness = int(1 * playfield_scale)
            if type & __CRIT_CIRCLE or type & __CRIT_SLIDER:
                circle_thickness = int(3 * playfield_scale)
                crit_color = (0, 0, crit_shade)  # red
            if type & __CRIT_SLIDER_TICK or type & __CRIT_SLIDER_END:
                circle_thickness = int(1 * playfield_scale)
                crit_color = (crit_shade, 0, 0)  # blue
            cv2.circle(img, crit_coord, int(20 * playfield_scale), crit_color, circle_thickness)
            # TODO: different shapes for different types of crit point (circle, slider, tick, end, etc...)
            # TODO: allow passing multiple ticks and it will process them similar to cursor trail, fade out if its further in future

    cursor_radius = int(2 * playfield_scale)
    use_line_trail = True

    draw_num = True
    for num, gameplay in enumerate(gameplays):  # TODO: be able to pass the model number to here and draw next to cursor
        circles_to_draw = len(gameplay)

        # front tick is where the cursor is, the rest are the tail
        cursor_path = [{'coord': (int(round(tick['coord_x'] * playfield_scale)) + int(pf_topleft[0]),
                                  int(round(tick['coord_y'] * playfield_scale)) + int(pf_topleft[1])),
                        'color': (int((255 / (circles_to_draw)) * (i + 1)),
                                  int((255 / (circles_to_draw)) * (i + 1)),
                                  int((255 / (circles_to_draw)) * (i + 1)))}
                      for i, tick in enumerate(gameplay[::-1])]
        #for i, tick in enumerate(gameplay[::-1]):
            #circle_center_coord = (int(round(tick['coord_x'] * playfield_scale)) + int(pf_topleft[0]),
            #                       int(round(tick['coord_y'] * playfield_scale)) + int(pf_topleft[1]))
            #circle_shade = int((255 / (circles_to_draw)) * (i + 1))
            #circle_color = (circle_shade, circle_shade, circle_shade)
            #cv2.circle(img, circle_center_coord, cursor_radius, circle_color, -1)
        for i, trail_point in enumerate(cursor_path[:-2]):  # TODO: could be clearer, could use range instead of enumeration
            cv2.line(img, cursor_path[i+1]['coord'], trail_point['coord'], trail_point['color'], cursor_radius)
            
        cv2.circle(img, cursor_path[0]['coord'], cursor_radius, cursor_path[0]['color'], -1)
        if draw_num:
            cv2.putText(img, str(num), cursor_path[-1]['coord'], cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

    return img

# display the "OpenH264 Video Codec provided by Cisco Systems, Inc." message upon importing render.py
__cv2_h264 = cv2.VideoWriter_fourcc('h', '2', '6', '4')
__cv2_t = cv2.VideoWriter('__test_.avi', __cv2_h264, 60.0, (640, 480))
__cv2_t.release()
os.remove('__test_.avi')