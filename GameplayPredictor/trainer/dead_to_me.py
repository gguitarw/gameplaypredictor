﻿from trainer.model import *
from trainer.predict import *
#from trainer.render import *
from trainer.render_2 import render_2
from time import clock
import os

def train_and_predict_and_render():
    # model
    features, labels = load_dataset('data\\DeadToMe.csv')
    num_features = len(features[0])
    num_labels = len(labels[0])
    layers = [(None, 'relu'),
              (100, 'relu'),
              (100, 'relu'),
              (100, 'relu')]
    model = define_model(num_features, num_labels, layers=layers)
    epochs = 5
    model = train_model(model, features, labels, epochs=epochs)

    # predict
    crit_points = load_crit_points('data\\DeadToMe.ocp')
    gameplay_ticks = generate_gameplay(model, crit_points)
    write_gameplay_ticks_to_file(gameplay_ticks, 'data\\DeadToMe.ogt')

    # render
    output_resolution = (640, 480)
    output_resolution = (1920, 1080)
    fps = 60.0
    render(gameplay_ticks, img_size=output_resolution, fps=fps)

def predict_and_render():
    model = load_model('models\\test_model_x_y.h5')
    
    # predict
    crit_points = load_crit_points('data\\DeadToMe.ocp')
    gameplay_ticks = generate_gameplay(model, crit_points)
    write_gameplay_ticks_to_file(gameplay_ticks, 'data\\DeadToMePreTrained.ogt')

    # render
    output_resolution = (640, 480)
    output_resolution = (1920, 1080)
    fps = 60.0
    render(gameplay_ticks, 'video_output\output_pretrained.avi', img_size=output_resolution, fps=fps)

def just_render():
    gameplay_ticks = load_gameplay_ticks_from_file('data\\DeadToMePreTrained.ogt')

    critical_points = load_crit_points('data\\DeadToMe.ocp')
    
    # render
    output_resolution = (640, 480)
    output_resolution = (1920, 1080)
    fps = 60.0
    render(gameplay_ticks, 'video_output\output_pre-predicted.avi', img_size=output_resolution, fps=fps,
           cursor_trail_length=20, critical_points=critical_points, use_skin_cursor=True)

def just_render_2():
    gameplays = [load_gameplay_ticks_from_file('data\\DeadToMePreTrained.ogt')]
    critical_points = load_crit_points('data\\DeadToMe.ocp')
    
    # render
    output_resolution = (800, 600)
    output_resolution = (640, 480)
    #output_resolution = (1920, 1080)
    fps = 60.0
    render_2(gameplays, 'video_output\output_pre-predicted_2.avi', img_size=output_resolution, fps=fps,
             cursor_trail_length=20, critical_points=critical_points, use_skin_cursor=False)

def just_render_multiple():
    files = ['data\\DeadToMe.ogt', 'data\\DeadToMePreTrained.ogt']
    gameplays = [load_gameplay_ticks_from_file(file) for file in files]

    critical_points = load_crit_points('data\\DeadToMe.ocp')

    # render
    output_resolution = (640, 480)
    #output_resolution = (1920, 1080)
    fps = 60.0
    render_2(gameplays, 'video_output\output_pre-predicted_multi.avi', img_size=output_resolution, fps=fps,
             cursor_trail_length=20, critical_points=critical_points)

def just_train_and_predict():
    # model
    features, labels = load_dataset('data\\DeadToMe.csv')
    num_features = len(features[0])
    num_labels = len(labels[0])
    layers = [(None, 'relu'),
              (100, 'relu'),
              (100, 'relu'),
              (100, 'relu')]
    model = define_model(num_features, num_labels, layers=layers)
    epochs = 5
    model = train_model(model, features, labels, epochs=epochs)

    # predict
    crit_points = load_crit_points('data\\DeadToMe.ocp')
    gameplay_ticks = generate_gameplay(model, crit_points)
    write_gameplay_ticks_to_file(gameplay_ticks, 'data\\DeadToMe.ogt')


#just_render()
#just_render_2()
#just_train_and_predict()
#just_render_multiple()
#predict_and_render()
#train_and_predict_and_render()
#exit() 

# TODO: the stuff below here should be moved into a batch.py or something
# TODO: make a class for model_details, really need some more object-oriented design here

def save_model_logs(model_deets, num, num_features, num_labels, training_time):
    with open('models\\batch_model_{}.log'.format(num), 'w') as f:
        f.write('Loss: {}\n'.format(model_deets['loss']))
        f.write('Optimization: {}\n'.format(model_deets['optimizer']))
        f.write('Epochs: {}\n'.format(model_deets['epochs']))
        f.write('Total time: {} (time per epoch avg: {})\n'.format(training_time, training_time / model_deets['epochs']))
        f.write('\nLayers:\n')
        # input layer
        layer = model_deets['layers'][0]
        f.write('\tInput layer: {} nodes, \'{}\' activation\n'.format(num_features, layer[1]))
        #hidden layers
        for i, layer in enumerate(model_deets['layers'][1:]):
            f.write('\tHidden layer #{}: {} nodes, \'{}\' activation\n'.format(i, layer[0], layer[1]))
        # output layers
        f.write('\tOutput layer: {} nodes'.format(num_labels))


def train_batch(dataset_path, model_details, save=False):
    # model_details is a list, each element contains a list of details for a model
    # summary: model_details[{'epochs': 5, 'layers': [(None, 'relu'), (100, 'relu')]}, {...}, ...]
    # TODO: for right now, just use epochs and layer details, add default and optionals for optimizer, dropout, validation, etc, later

    # model
    features, labels = load_dataset(dataset_path)
    num_features = len(features[0])
    num_labels = len(labels[0])

    models = []
    for i, model_deets in enumerate(model_details):
        model = define_model(num_features, num_labels, model_deets['layers'],
                             optimizer=model_deets['optimizer'], loss=model_deets['loss'])
        t0 = clock()
        model = train_model(model, features, labels, model_deets['epochs'])
        t1 = clock()
        models.append(model)
        if save:
            save_model(model, 'models\\batch_model_{}'.format(i))
            save_model_logs(model_deets, i, num_features, num_labels, t1-t0)
        
    # TODO: also output a log file indication the configuration for each model (to identify model 1 out of 20, etc.)
    return models

# TODO: let these be able to load from file, with optional bool that then interprets 'models' as a path/directory to load from    
def predict_batch(models, crit_point_path, save=False):
    # predict
    crit_points = load_crit_points(crit_point_path)

    gameplays = []
    for i, model in enumerate(models):
        gameplay_ticks = generate_gameplay(model, crit_points)
        gameplays.append(gameplay_ticks)
        if save:
            write_gameplay_ticks_to_file(gameplay_ticks, 'gameplays\\batch_predict_{}.ogt'.format(i))
    return gameplays

def render_batch(gameplays, output_directory, critical_points_path=None, output_resolution=(640, 480),
                 render_together=True, render_individual=False):
    critical_points = None
    if critical_points_path is not None:
        critical_points = load_crit_points(critical_points_path)
    
    # render
    #output_resolution = (1920, 1080)

    fps = 60.0
    if render_individual:
        for i, gameplay in enumerate(gameplays):
            render_2(gameplay, 'render\\batch_render_{}.avi'.format(i), img_size=output_resolution, fps=fps,
                     cursor_trail_length=20, critical_points=critical_points)
    if render_together:
        render_2(gameplays, 'renders\\batch_render_multi.avi', img_size=output_resolution, fps=fps,
                 cursor_trail_length=10, critical_points=critical_points)
    return

def setup_models():
    model_details = []

    epoch_trials = [1, 2, 5, 10, 20, 50]
    epoch_trials = [1]  # TEMPORARY, JUST USE WHILE TESTING
    optimizers = [
        #'sgd',
        'rmsprop',
        'adagrad',
        #'adadelta',
        #'adamax',
        #'nadam',
        'adam',
    ]
    loss_functions = [
        'mean_squared_error',
        #'mean_absolute_percentage_error',
        #'mean_squared_logarithmic_error',
        #'squared_hinge',
        #'hinge',
        #'categorical_hinge',
        #'logcosh',
        #'kullback_leibler_divergence',
        #'poisson',
        #'cosine_proximity',
        'mean_absolute_error',
    ]
    activations = [
        'relu',
        #'softmax',
        #'elu',
        #'selu',
        #'softplus',
        #'softsign',
        'tanh',
        #'sigmoid',
        #'hard_sigmoid',
        'linear',
    ]

    node_counts = [2, 10, 100]#, 1000]
    hidden_layer_counts = [1, 2, 3, 5]#, 10, 50]
    layer_configs = []

    for activation_func in activations:
        for node_count in node_counts:
            for hidden_count in hidden_layer_counts:
                layer_config = [(None, activation_func)]  # input layer
                for _ in range(hidden_count):
                    layer_config.append((node_count, activation_func))
                layer_configs.append(layer_config)

    #for node_count in node_counts:
    #    for activation_1 in activations:
    #        for activation_2 in activations:
    #            layer_config = []  # TODO


    for epoch_count in epoch_trials:
        for loss_func in loss_functions:
            for optimizer_func in optimizers:
                for layer_config in layer_configs:
                    epochs = epoch_count
                    loss = loss_func
                    optimizer = optimizer_func
                    layers = layer_config

                    model_deets = {'epochs': epochs,
                                   'loss': loss,
                                   'optimizer': optimizer,
                                   'layers': layers}
                    model_details.append(model_deets)

    print('Done generating {} model configurations'.format(len(model_details)))
    return model_details
