import setuptools
from setuptools import find_packages
from setuptools import setup

REQUIRED_PACKAGES = ['tensorflow', 'numpy', 'h5py']

setup(
    name='GameplayPredictor',
    version='0.1',
    install_requires=REQUIRED_PACKAGES,
    packages=find_packages(),
    include_package_data=true,
    description='Predicts gameplay'
)