import os
from trainer.model import *
from trainer.predict import *
#from trainer.render import *
from trainer.render_2 import render_2
import gc

def predict_batch(models, crit_point_path, load_from_file=False, save=False):
    # predict
    crit_points = load_crit_points(crit_point_path)

    if load_from_file:
        models_path = models
        models = []
        with os.scandir(models_path) as it:
            for entry in it:
                if entry.name.startswith('batch_model_') and entry.name.endswith('.h5'):
                    model_num = entry.name[len('batch_model_'):-1*len('.h5')]
                    loaded_model = load_model(str(os.path.join(models_path, entry.name)))
                    print('Loaded model \'{}\''.format(entry.name))
                    gameplay_ticks = generate_gameplay(loaded_model, crit_points)
                    write_gameplay_ticks_to_file(gameplay_ticks, 'gameplays\\batch_predict_{}.ogt'.format(model_num))
                    #models.append(loaded_model)
                gc.collect()
    else:
        gameplays = []
        for i, model in enumerate(models):
            gameplay_ticks = generate_gameplay(model, crit_points)
            gameplays.append(gameplay_ticks)
            if save:
                write_gameplay_ticks_to_file(gameplay_ticks, 'gameplays\\batch_predict_{}.ogt'.format(i))
        return gameplays

dataset_path = 'data\\DeadToMe.csv'
models_dir = 'models\\'

predict_batch(models_dir, 'data\\DeadToMe.ocp', load_from_file=True, save=True)
