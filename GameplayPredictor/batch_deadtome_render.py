
import os
from trainer.predict import *
#from trainer.render import *
from trainer.render_2 import render_2

# TODO: batch size these
def render_batch(gameplays, output_directory, critical_points_path=None, output_resolution=(640, 480),
                 load_from_file=False,
                 render_together=True, render_individual=False):
    if load_from_file:
        gameplays_path = gameplays
        gameplays = []

        with os.scandir(gameplays_path) as it:
            for entry in it:
                if entry.name.startswith('batch_predict_') and entry.name.endswith('.ogt'):
                    loaded = load_gameplay_ticks_from_file(str(os.path.join(gameplays_path, entry.name)))
                    print('Loaded gameplays ticks from \'{}\''.format(entry.name))
                    gameplays.append(loaded)

    critical_points = None
    if critical_points_path is not None:
        critical_points = load_crit_points(critical_points_path)
    
    # render
    output_resolution = (1920, 1080)

    fps = 60.0
    if render_individual:
        for i, gameplay in enumerate(gameplays):
            # TODO: keep the same model number from the prediction here (might require using another dict)
            render_2([gameplay], 'renders\\batch_render_{}.avi'.format(i), img_size=output_resolution, fps=fps,
                     cursor_trail_length=20, critical_points=critical_points)
    if render_together:
        render_2(gameplays, 'renders\\batch_render_multi.avi', img_size=output_resolution, fps=fps,
                 cursor_trail_length=10, critical_points=critical_points)
    return

dataset_path = 'data\\DeadToMe.csv'
gameplays_dir = 'gameplays\\small\\'

render_batch(gameplays_dir, critical_points_path='data\\DeadToMe.ocp', load_from_file=True)#, render_together=True, render_individual=True)

#models = train_batch(dataset_path, model_details, save=True)
#gameplays = predict_batch(models, crit_point_path)
#render_batch(gameplays, output_directory)


#gameplays = [load_gameplay_ticks_from_file('gameplays\\batch_predict_0.ogt')]
#critical_points = load_crit_points('data\\DeadToMe.ocp')
    
## render
#output_resolution = (800, 600)
#output_resolution = (640, 480)
##output_resolution = (1920, 1080)
#fps = 60.0
#render_2(gameplays, 'renders\\batch_render_0.avi', img_size=output_resolution, fps=fps,
#            cursor_trail_length=20, critical_points=critical_points, use_skin_cursor=False)